const express = require("express");
const mongoose = require("mongoose");
const redis = require("redis");

const app = express();
const PORT = 3004;

// Start server
app.listen(PORT, () => {
  console.log(`App running on http://localhost:${PORT}`);
});
/* connection for mongoose */
mongoose
  .connect(
    "mongodb+srv://nestamaulana09:4WbOmNUwLmq13Uhk@dbnestabetest.zfqy9ez.mongodb.net/DB_NestaMaulana_Test?retryWrites=true&w=majority&appName=dbnestabetest",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => {
    console.log("MongoDB connected successfully");
  })
  .catch((error) => {
    console.error("MongoDB connection error:", error);
  });

// Routes
const userRoutes = require("./routes/userRoute");
const authRoutes = require("./routes/authRoute"); 
app.use(express.json());

app.get("/", (req, res) => {
  res.send(`Hello this application running on port: ${PORT}`);
});
app.use("/users", userRoutes);
app.use("/auth", authRoutes); 