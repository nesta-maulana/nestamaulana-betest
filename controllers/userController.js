// controllers/userController.js

const User = require("../models/user.model");
const redis = require("redis");
const client = redis.createClient({
  socket: {
    host: "redis",
  },
  port: "6379",
  password: "",
  database: "0",
});
client.connect();
client.on("connect", () => {
  console.log("Connected to Redis!");
});
client.on("error", (err) => {
  console.error("Redis error:", err);
});

// Create User
exports.createUser = async (req, res) => {
  try {
    const newUser = await User.create(req.body);
    res.status(201).json(newUser);
  } catch (err) {
    if (err.code === 11000 && err.keyPattern.accountNumber === 1) {
      res.status(400).json({ message: "Nomor akun sudah ada di database." });
    } else if (err.code === 11000 && err.keyPattern.emailAddress === 1) {
      res.status(400).json({ message: "Alamat email sudah ada di database." });
    } else if (err.code === 11000 && err.keyPattern.identityNumber === 1) {
      res
        .status(400)
        .json({ message: "Nomor identitas sudah ada di database." });
    } else {
      res.status(400).json({ message: err.message });
    }
  }
};

// Get All Users
exports.getAllUsers = async (req, res) => {
  try {
    if (client.isOpen) {
      console.log("Checking Redis cache for all users...");
      const data = await client.get("datauser");
      if (data !== null) {
        console.log("Users found in cache!");
        res.json(JSON.parse(data));
        return; // Exit after sending cached users
      }

      console.log("Users not found in cache!");
      const users = await User.find();
      await client.set("datauser", JSON.stringify(users), "EX", 60 * 10);
      console.log("Users saved to cache!");
      res.json(users);
    } else {
      console.error("Redis connection is closed!");
      res.status(500).json({
        message: "Internal Server Error: Redis connection is closed",
      });
    }
  } catch (error) {
    console.error("Error fetching users:", error);
    // Handle specific errors here (e.g., Redis errors, database errors)
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Get User by ID
exports.getUserById = async (req, res) => {
  try {
    const userId = req.params.id;
    if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
      return res.status(404).json({ message: "User not found" });
    }

    const user = await User.findById(userId);
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ message: "User not found" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
exports.getUserByAccountNumber = async (req, res) => {
  try {
    const accountNumber = req.params.accountNumber;
    const user = await User.findOne({ accountNumber: accountNumber });
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ message: "User not found" + accountNumber });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
exports.getUserByIdentityNumber = async (req, res) => {
  try {
    const identityNumber = req.params.identityNumber;
    const user = await User.findOne({ identityNumber: identityNumber });
    if (user) {
      res.json(user);
    } else {
      res.status(404).json({ message: "User not found" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Update User by ID
exports.updateUserById = async (req, res) => {
  try {
    const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (updatedUser) {
      res.json({ message: "Data berhasil diupdate", user: updatedUser });
    } else {
      res.status(404).json({ message: "User not found" });
    }
  } catch (err) {
    if (err.code === 11000 && err.keyPattern.accountNumber === 1) {
      res.status(400).json({ message: "Nomor akun sudah ada di database." });
    } else if (err.code === 11000 && err.keyPattern.emailAddress === 1) {
      res.status(400).json({ message: "Alamat email sudah ada di database." });
    } else if (err.code === 11000 && err.keyPattern.identityNumber === 1) {
      res
        .status(400)
        .json({ message: "Nomor identitas sudah ada di database." });
    } else {
      res.status(500).json({ message: err.message });
    }
  }
};

// Delete User by ID
exports.deleteUserById = async (req, res) => {
  try {
    const deletedUser = await User.findByIdAndDelete(req.params.id);
    if (deletedUser) {
      res.json({ message: "User deleted" });
    } else {
      res.status(404).json({ message: "User not found" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
