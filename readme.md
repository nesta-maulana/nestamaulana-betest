Project Installation Guide

Requirements:
- Node.js v20 or higher
- Docker

Installation Steps:

1. Clone the Repository
   - Clone the project repository to your local machine.
     ```
     git clone https://gitlab.com/nesta-maulana/nestamaulana-betest
     ```

2. Check Port Availability
   - Before running the project, ensure that ports 3004 and 6379 are not already in use by other services on your environment.

3. Build and Run with Docker Compose
   - Navigate to the project directory and run the following command to build and run the project using Docker Compose:
     ```
     docker-compose up --build
     ```
   - This command will build the Docker images and start the containers specified in the `docker-compose.yml` file. The `--build` flag ensures that Docker Compose builds the images before starting the containers.

4. Access API Documentation
   - After the containers are up and running, you can access the API documentation here: [API Documentation](https://documenter.getpostman.com/view/12716985/2sA3BrXV6b).
