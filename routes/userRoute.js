const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const authenticateToken = require("../middlewares/authenticateToken");

router.use(authenticateToken);
router.post("/", userController.createUser);
router.get("/", userController.getAllUsers);
router.get("/find-by-id/:id", userController.getUserById);
router.get("/find-by-account-number/:accountNumber", userController.getUserByAccountNumber);
router.get("/find-by-identity-number/:identityNumber", userController.getUserByIdentityNumber);
router.patch("/:id", userController.updateUserById);
router.delete("/:id", userController.deleteUserById);

module.exports = router;
